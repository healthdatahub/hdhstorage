#! /usr/bin/env python
# -*- coding: utf-8 -*-

import logging

import pytest

from hdhactions.storage import copy_between_storages

logger = logging.getLogger(__name__)


@pytest.fixture()
def setup(storageTest):
    # Setup directories for unit testing
    yield storageTest.create_directory(directory="unittest")


@pytest.fixture()
def setup_multi_accounts_with_contents(storageTest):
    """Setup storageTest for cross-container transfers.

    Once the fixture is run, storageTest contains the following containers:
    - "inputdir" contains
        - "a.txt": "AAA-in"
        - "b.txt": "BBB-in"
    - "outputdir" contains
        - "a.txt": "AAA-out"

    storageTest is guaranteed to be empty at instanciation, hence no other
    containers / files should be present.

    Returns the first four arguments of utils.copy_between_storages (as a dict).
    """

    # check storageTest is empty
    assert len(storageTest.list_directories()) == 0

    sdn = "inputdir"
    tdn = "outputdir"

    exitdic = dict(
        source_storage=storageTest,
        target_storage=storageTest,
        source_directory=sdn,
        target_directory=tdn,
    )

    # create directories
    storageTest.create_directory(directory=sdn)
    storageTest.create_directory(directory=tdn)

    # Put the content inside it
    data_to_fill = [
        (sdn, "a.txt", "AAA-in"),
        (sdn, "b.txt", "BBB-in"),
        (tdn, "a.txt", "AAA-out"),
    ]

    for directory_name, filename, data in data_to_fill:
        storageTest.push_data(
            directory=directory_name, remote_filename=filename, data=data
        )

    return exitdic


def test_cross_storage_transfer_overwrite_not_allowed(
    setup_multi_accounts_with_contents,
):
    # Try to transfer a file that already exists
    ca = setup_multi_accounts_with_contents

    with pytest.raises(ValueError):
        copy_between_storages(
            **ca,
            source_files=["a.txt"],
            # overwrite_ok=False, # implied by defaults
        )


def test_cross_storage_transfer_file_not_exist(setup_multi_accounts_with_contents):
    # Try to transfer a file that does not exist
    ca = setup_multi_accounts_with_contents

    # Trying to copy a non-existent file when argument does not allow to skip
    with pytest.raises(ValueError):
        copy_between_storages(
            **ca,
            source_files=["b.txt", "I_dont_exist.txt"],
            # missing_ok=False, # implied by defaults
        )

    # Check that b.txt was not written
    assert "b.txt" not in ca["target_storage"].list_files(ca["target_directory"])


def test_cross_storage_skip_working(setup_multi_accounts_with_contents):
    # Actually copy a file when argument contains a file to skip
    ca = setup_multi_accounts_with_contents

    outstorage = ca["target_storage"]
    outdir = ca["target_directory"]

    logger.debug(f"TEASTY: {outstorage.list_files(outdir)}")

    fname = "b.txt"
    expected_content = "BBB-in"

    copy_between_storages(
        **ca, source_files=["I_dont_exist.txt", fname], missing_ok=True
    )

    assert fname in outstorage.list_files(outdir)
    copied_txt = outstorage.read_file(
        directory=outdir, filename=fname, fmt="txt", zip=False
    )

    assert copied_txt == expected_content


def test_cross_storage_transfer_working(setup_multi_accounts_with_contents):
    # Actually copy a file when nothing is blocking the transfer
    ca = setup_multi_accounts_with_contents

    storage = ca["target_storage"]
    outdir = ca["target_directory"]
    fname = "b.txt"
    expected_content = "BBB-in"

    copy_between_storages(
        **ca,
        source_files=[fname],
    )

    assert fname in storage.list_files(outdir)
    copied_txt = storage.read_file(
        directory=outdir, filename=fname, fmt="txt", zip=False
    )

    assert copied_txt == expected_content


def test_cross_storage_overwrite_working(setup_multi_accounts_with_contents):
    # Actually copy a file with overwrite
    ca = setup_multi_accounts_with_contents

    storage = ca["target_storage"]
    outdir = ca["target_directory"]
    fname = "a.txt"
    expected_content = "AAA-in"

    copy_between_storages(**ca, source_files=[fname], overwrite_ok=True)

    assert fname in storage.list_files(outdir)
    copied_txt = storage.read_file(
        directory=outdir, filename=fname, fmt="txt", zip=False
    )

    assert copied_txt == expected_content


def test_list_files(setup_multi_accounts_with_contents):
    ca = setup_multi_accounts_with_contents

    outstor = ca["target_storage"]
    outdir = ca["target_directory"]

    assert isinstance(outstor.list_files(outdir, True), list)
    assert isinstance(outstor.list_files(outdir, False), list)


# TODO: add metadata test?
