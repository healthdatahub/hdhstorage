# -*- coding: utf-8 -*-
import pytest


@pytest.fixture()
def setup_softdelete(storageTest):
    # Setup directories for unit testing
    tempname = "tmp"
    yield storageTest.create_directory(directory=tempname)


# tests not done yet, beacause the feature SoftDelete/versioning is not avaibale in azuite
# solution: add a condition if we are on azurite skip the tests, else execute them
# other solution: see comments on git issue#3


def test_deleted(setup_softdelete):
    pass


def test_list_deleted(setup_softdelete):
    pass


def test_restored(setup_softdelete):
    pass
